var functionToExecute = '';

function copyContentsToClipboard(element) {
    navigator.clipboard.writeText(element.textContent);
}

function setupMenu() {
    for (let menuItem of MENU) {
        const element = document.createElement('p');
        element.classList.add('glowOnHover');
        element.textContent = '[' + menuItem.name + ']';
        element.dataset.info = menuItem.name;
        element.onclick = menuBarElementOnClick;
        document.getElementById('menuBar').appendChild(element);
    }
}

function setCurrentScript(scriptName) {
    functionToExecute = 'f' + scriptName;

    let currentScriptDisplay = document.getElementById("currentScript");
    currentScriptDisplay.innerText = "Current Script: " + scriptName;
    localStorage.setItem("selectedScript", scriptName);
}

function menuBarElementOnClick() {
    let tasksBar = document.getElementById('tasks');
    tasksBar.innerHTML = '';

    let text = this.dataset.info;
    for (let data of MENU.find((value) => value.name === text).content) {
        const element = document.createElement('p');
        element.classList.add('glowOnHover');
        element.textContent = '[' + data + ']';
        tasksBar.appendChild(element);
        element.onclick = () => {
            setCurrentScript(text + data);
        }
    }
}

window.onload = () => {
    setupMenu();
    let input = localStorage.getItem("input");
    if (input) {
        document.getElementById('codeInput').value = input;
    }
    let selectedScript = localStorage.getItem("selectedScript");
    setCurrentScript(selectedScript);
}

// This could be expensive. worth keeping an eye on it to see if it needs to be called less
function saveData(textArea) {
    localStorage.setItem("input", textArea.value);
}

function processInput() {
    console.log("trying to call " + functionToExecute);
    let input = document.getElementById('codeInput').value;
    
    let beforeTime = window.performance.now();
    // Removing the try catch because the crash gives better information
    let output = window[functionToExecute](input);
    let afterTime =  window.performance.now();

    // to the nearest 0.1ms
    console.log("ran in " + Math.round((afterTime - beforeTime)*10)/10 + "ms");

    document.getElementById('outputBox').innerHTML = output;
}