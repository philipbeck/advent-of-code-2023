function ftestsSuccesfulText(input)
{
    return 'testSuccessful';
}

function ftestsCharacterCount(input)
{
    return input.length;
}

// For copying when making new functions
function ftestsTemplate(input)
{
    let lines = input.split("\n");
    return "Not Implemented";
}

function ftestsMultipleLineOutput(input)
{
    let text = "this is a \nexample of code\nwith multiple lines\nto output";
    text = text.replace(/\n/g, "<br>");
    console.log(text);
    return text;
}