function dec2bin(dec) {
    return (dec >>> 0).toString(2);
}

function replaceChar(str, c, i) {
    return str.substring(0, i) + c + str.substring(i + 1);
}

function f2020day14part1(input) {
    let lines = input.split("\n");

    let mask;
    let memoryMap = new Map();
    for (line of lines) {
        let data = line.split(" = ");
        if (data[0] == "mask") {
            mask = data[1];
            continue;
        }

        let memPos = data[0].split(/\D+/g)[1];

        let memoryData = "" + dec2bin(data[1]);
        while (memoryData.length != 36) {
            memoryData = "0" + memoryData;
        }

        for (let i = 0; i < memoryData.length; i++) {
            let maskI = memoryData.length - i - 1;
            let c = mask[maskI];
            if (c != "X") {
                memoryData = replaceChar(memoryData, c, maskI);
            }
        }
        memoryMap.set(memPos, memoryData);
    }

    let total = 0;
    memoryMap.forEach((value, key) => {
        total += parseInt(value, 2);
    })
    return total;
}

function f2020day14part2(input) {
    let lines = input.split("\n");

    let mask;
    let masks;
    let memoryMap = new Map();
    for (line of lines) {
        let data = line.split(" = ");
        if (data[0] == "mask") {
            mask = data[1];

            masks = [mask];

            for (let i = 0; i < mask.length; i++) {
                if (mask[i] == "X") {
                    masks = masks.concat(masks);
                    for (let j = 0; j < masks.length; j++) {
                        masks[j] = replaceChar(masks[j], (j < masks.length / 2) ? 0 : 1, i);
                    }
                }
                if (mask[i] == 0) {
                    for (let j = 0; j < masks.length; j++) {
                        masks[j] = replaceChar(masks[j], "-", i);
                    }
                }
            }
            continue;
        }
        let memPos = data[0].split(/\D+/g)[1];

        for (let m of masks) {
            let address = "" + dec2bin(memPos);
            while (address.length != 36) {
                address = "0" + address;
            }

            for (let i = 0; i < address.length; i++) {
                let maskI = address.length - i - 1;
                let c = m[maskI];
                if (c != "-") {
                    address = replaceChar(address, c, maskI);
                }
            }

            memoryMap.set(address, dec2bin(data[1]));
        }
    }

    let total = 0;
    memoryMap.forEach((value, key) => {
        total += parseInt(value, 2);
    })
    return total;
}