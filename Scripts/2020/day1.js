function f2020day1part1(input) {
    let strings = input.split("\n");
    for (let i = 1; i < strings.length; i++) {
        for (let j = i - 1; j >= 0; j--) {
            let numI = Number(strings[i]);
            let numJ = Number(strings[j]);
            if (numI + numJ == 2020) {
                return numI * numJ;
            }
        }
    }
    return "answerNotFound";
}

function f2020day1part2(input) {
    let strings = input.split("\n");
    for (let i = 0; i < strings.length; i++) {
        for (let j = i + 1; j < strings.length; j++) {
            for (let k = j + 1; k < strings.length; k++) {
                let numI = Number(strings[i]);
                let numJ = Number(strings[j]);
                let numK = Number(strings[k]);
                if (numI + numJ + numK == 2020) {
                    return numI * numJ * numK;
                }
            }
        }
    }
    return "answerNotFound";
}