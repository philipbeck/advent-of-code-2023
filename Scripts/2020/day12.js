const DIRECTION_ORDER = [
    [1, 0],//north
    [0, -1],//west
    [-1, 0],//south
    [0, 1],//east
]

function turn(direction, degrees, facing) {
    if (direction == "R") {
        facing -= degrees / 90;
        if (facing < 0) {
            facing += 4;
        }
    }
    if (direction == "L") {
        facing += degrees / 90;
        if (facing > 3) {
            facing -= 4;
        }
    }
    return facing;
}

function f2020day12part1(input) {
    let lines = input.split("\n");
    let pos = [0, 0];
    let facing = 3;

    for (line of lines) {
        switch (line[0]) {
            case "N":
                pos[0] += Number(line.slice(1));
                break;
            case "E":
                pos[1] += Number(line.slice(1));
                break;
            case "S":
                pos[0] -= Number(line.slice(1));
                break;
            case "W":
                pos[1] -= Number(line.slice(1));
                break;
            case "R":
                facing = turn("R", Number(line.slice(1)), facing);
                break;
            case "L":
                facing = turn("L", Number(line.slice(1)), facing);
                break;
            case "F":
                pos[0] += DIRECTION_ORDER[facing][0] * Number(line.slice(1));
                pos[1] += DIRECTION_ORDER[facing][1] * Number(line.slice(1));
                break;
            default:
                break;
        }
    }
    return Math.abs(pos[0]) + Math.abs(pos[1]);
}

function rotateWayPoint(direction, degrees, wayPoint) {
    let newWayPoint = wayPoint;
    for (let i = 0; i < degrees/90; i++) {
        newWayPoint = [direction == "R" ? -newWayPoint[1] : newWayPoint[1], direction == "L" ? -newWayPoint[0] : newWayPoint[0]];
    }
    return newWayPoint;
}

function f2020day12part2(input) {
    let lines = input.split("\n");
    let wayPoint = [1, 10];
    let pos = [0, 0];

    for (line of lines) {
        switch (line[0]) {
            case "N":
                wayPoint[0] += Number(line.slice(1));
                break;
            case "E":
                wayPoint[1] += Number(line.slice(1));
                break;
            case "S":
                wayPoint[0] -= Number(line.slice(1));
                break;
            case "W":
                wayPoint[1] -= Number(line.slice(1));
                break;
            case "R":
            case "L":
                wayPoint = rotateWayPoint(line[0], Number(line.slice(1)), wayPoint);
                break;
            case "F":
                pos[0] += wayPoint[0] * Number(line.slice(1));
                pos[1] += wayPoint[1] * Number(line.slice(1));
                break;
            default:
                break;
        }
    }
    return Math.abs(pos[0]) + Math.abs(pos[1]);
}
