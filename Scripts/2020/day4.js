const REQUIRED_FIELDS = [
    'byr',
    'iyr',
    'eyr',
    'hgt',
    'hcl',
    'ecl',
    'pid',
]

function checkPassportPart1(passport) {
    for (field of REQUIRED_FIELDS) {
        if (!passport.includes(field)) {
            return false;
        }
    }
    return true;
}

function f2020day4part1(input) {
    let lines = input.split("\n");

    let passport = "";
    let validPassportCount = 0;
    for (line of lines) {
        if (line == "") {
            if (checkPassportPart1(passport)) {
                validPassportCount++;
            }
            passport = "";
        }
        else {
            passport += " " + line;
        }
    }
    if (checkPassportPart1(passport)) {
        validPassportCount++;
    }

    return validPassportCount;
}

function getTextAfter(field, passport) {
    let str = passport.split(field + ":")[1];
    return str.split(" ")[0];
}

function checkYears(field, passport, min, max) {
    let year = getTextAfter(field, passport);
    let yearNum = Number(year);
    if (!yearNum || yearNum < min || yearNum > max) {
        return false;
    }
    return true;
}

// This should probably be split into different functions
function checkPassportPart2(passport) {
    if (!checkPassportPart1(passport)) {
        return false;
    }
    // byr
    if (!checkYears("byr", passport, 1920, 2002)) {
        return false;
    }
    // iyr
    if (!checkYears("iyr", passport, 2010, 2020)) {
        return false;
    }
    // eyr
    if (!checkYears("eyr", passport, 2020, 2030)) {
        return false;
    }
    // hgt
    let hgt = getTextAfter("hgt", passport);
    let min;
    let max;
    let hgtNum;
    if (hgt.includes("cm")) {
        min = 150;
        max = 193;
        hgtNum = Number(hgt.split("cm")[0]);
    }
    else if (hgt.includes("in")) {
        min = 59;
        max = 76;
        hgtNum = Number(hgt.split("in")[0]);
    }
    else {
        return false;
    }

    if (hgtNum == NaN || hgtNum < min || hgtNum > max) {
        return false;
    }

    //hcl
    let hcl = getTextAfter("hcl", passport);
    if (!hcl.match(/#[\da-f]{6}/)) {
        return false;
    }

    // ecl
    let ecl = getTextAfter("ecl", passport);
    if (!ecl.match(/amb|blu|brn|gry|grn|hzl|oth/)) {
        return false;
    }

    // pid
    let pid = getTextAfter("pid", passport);
    if (pid.length != 9) {
        return false;
    }
    for (let n of pid) {
        if (Number(n) == NaN) {
            return false;
        }
    }

    // if everything passed return true;
    return true;
}


function f2020day4part2(input) {
    let lines = input.split("\n");

    let passport = "";
    let validPassportCount = 0;
    for (line of lines) {
        if (line == "") {
            if (checkPassportPart2(passport)) {
                validPassportCount++;
            }
            passport = "";
        }
        else {
            passport += " " + line;
        }
    }
    if (checkPassportPart2(passport)) {
        validPassportCount++;
    }

    return validPassportCount;
}