function f2020day13part1(input) {
    let lines = input.split("\n");
    let minTime = Number(lines[0]);
    let busNumbers = lines[1].split(/\D+/g);
    let minDiff = Number.MAX_VALUE;
    let minBusNumber = Number.MAX_VALUE;
    for (let n of busNumbers) {
        let busNumber = Number(n);
        //just incase they can start with an X
        if (isNaN(busNumber)) {
            continue;
        }
        let timeAfter = Math.ceil(minTime / busNumber) * busNumber;
        if (timeAfter - minTime < minDiff) {
            minDiff = timeAfter - minTime;
            minBusNumber = busNumber;
        }
    }
    return minBusNumber * minDiff;
}

function getIntersection(n1, n2, firstPosition, interval) {
    for (let i = firstPosition; true; i += Number(n1)) {
        if ((i + interval)%n2 == 0)
        {
            return i + interval;
        }
    }
}

function f2020day13part2(input) {
    let lines = input.split("\n");

    let busNumbers = lines[1].split(",");
    let currentNumber = busNumbers[0]
    let firstPosition = 0;
    let interval = 0;
    for (let i = 1; i < busNumbers.length; i++) {
        let busNumber = Number(busNumbers[i]);
        if (isNaN(busNumber)) {
            continue;
        }
        firstPosition = getIntersection(currentNumber, busNumber, firstPosition, i - interval);
        interval = i;
        currentNumber = currentNumber * busNumber;
    }
    return firstPosition - busNumbers.length + 1;
}