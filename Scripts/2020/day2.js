function f2020day2part1(input) {
    let strings = input.split("\n");
    let validPasswords = 0;
    for (let line of strings) {
        let splitStrings = line.split('-');
        let min = Number(splitStrings[0]);
        line = splitStrings[1];
        splitStrings = line.split(' ');
        let max = Number(splitStrings[0]);
        let char = splitStrings[1][0];
        let password = splitStrings[2];

        let charCount = 0;
        for(let c of password)
        {
            if(c == char)
            {
                charCount++;
                if(charCount > max)
                {
                    break;
                }
            }
        }

        if(min <= charCount && max >= charCount)
        {
            validPasswords++;
        }
    }
    return validPasswords;
}

function f2020day2part2(input) {
    let strings = input.split("\n");
    let validPasswords = 0;
    for (let line of strings) {
        let splitStrings = line.split('-');
        let pos1 = Number(splitStrings[0]) - 1;
        line = splitStrings[1];
        splitStrings = line.split(' ');
        let pos2 = Number(splitStrings[0]) - 1;
        let char = splitStrings[1][0];
        let password = splitStrings[2];

        if((password[pos1] == char) != (password[pos2] == char))
        {
            validPasswords++;
        }
    }
    return validPasswords;
}