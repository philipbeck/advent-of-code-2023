function checkSlope(lines, vertical, horizontal)
{
    let treeCount = 0;
    let j = 0;
    for(let i = vertical; i < lines.length; i += vertical)
    {
        j += horizontal;
        if(lines[i][j%lines[i].length] == '#')
        {
            treeCount++;
        }
    }
    return treeCount;
}

function f2020day3part1(input)
{
    let lines = input.split("\n");
    return checkSlope(lines, 1, 3);
}

function f2020day3part2(input)
{
    let lines = input.split("\n");
    let totalValue = checkSlope(lines, 1, 1);
    totalValue *= checkSlope(lines, 1, 3);
    totalValue *= checkSlope(lines, 1, 5);
    totalValue *= checkSlope(lines, 1, 7);
    totalValue *= checkSlope(lines, 2, 1);
    return totalValue;
}