function containsGold(name, bagMap) {
    if (bagMap.has(name)) {
        if (bagMap.get(name).length == 0) {
            return false;
        }
        for (let item of bagMap.get(name)) {
            if (item.colour == "shiny gold" || containsGold(item.colour, bagMap)) {
                return true;
            }
        }
    }
    return false;
}

function f2020day7part1(input) {
    let lines = input.split("\n");

    let bagMap = new Map();

    for (line of lines) {
        let info = line.split(/ bags?,?(?: contain)? ?/);
        //console.log(info);
        let name = info[0];
        let contents = [];
        for (let item of info) {
            if (item == "no other") {
                break;
            }
            if (item == name || item == ".") {
                continue;
            }
            contents.push({ count: Number(item[0]), colour: item.slice(2) });
        }
        console.log(contents);
        bagMap.set(name, contents);
    }

    let total = 0;
    bagMap.forEach((value, key) => {
        console.log(value);
        if (containsGold(key, bagMap)) {
            total++;
        }
    });

    return total;
}

function countBags(name, bagMap) {
    let bagsInside = 1;
    if (bagMap.has(name)) {
        if (bagMap.get(name).length == 0) {
            return 1;
        }
        for (let item of bagMap.get(name)) {
            bagsInside += item.count * countBags(item.colour, bagMap);
        }
    }
    console.log(bagsInside);
    return bagsInside;
}

function f2020day7part2(input) {
    let lines = input.split("\n");

    let bagMap = new Map();

    for (line of lines) {
        let info = line.split(/ bags?,?(?: contain)? ?/);
        //console.log(info);
        let name = info[0];
        let contents = [];
        for (let item of info) {
            if (item == "no other") {
                break;
            }
            if (item == name || item == ".") {
                continue;
            }
            contents.push({ count: Number(item[0]), colour: item.slice(2) });
        }
        console.log(contents);
        bagMap.set(name, contents);
    }

    return countBags("shiny gold", bagMap) - 1;
}