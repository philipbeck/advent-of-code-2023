function f2020day8part1(input) {
    let lines = input.split("\n");

    let usedLines = new Map();
    let acc = 0;
    for (let i = 0; i < lines.length; i++) {
        let line = lines[i];

        let command = line.split(" ")[0];
        let sign = lines[i][4];
        let n = Number(line.split(/[-+]/)[1]);

        if (usedLines.has(i)) {
            return acc;
        }
        usedLines.set(i, true);
        switch (command) {
            case "jmp":
                i--;
                i += (sign == "-") ? -n : n;
                break;
            case "acc":
                acc += (sign == "-") ? -n : n;
                break;
            case "nop":
            default:
                break;
        }
    }

    return "Something went wrong";
}

function f2020day8part2(input) {
    let lines = input.split("\n");

    let acc = 0;

    for (let j = 0; j < lines.length; j++) {
        let usedLines = new Map();
        let jmpNopCounter = 0;
        acc = 0;

        let finished = true;
        for (let i = 0; i < lines.length; i++) {
            let line = lines[i];

            let command = line.split(" ")[0];
            let sign = lines[i][4];
            let n = Number(line.split(/[-+]/)[1]);

            if (jmpNopCounter == j) {
                if (command == "jmp") {
                    command = "nop";
                }
                else if (command == "nop") {
                    command = "jmp";
                }
            }

            if (usedLines.has(i)) {
                finished = false;
                break;
            }
            usedLines.set(i, true);
            console.log("pos: " + i + " " + command + " " + sign + "" + n);
            switch (command) {
                case "jmp":
                    i--;
                    i += (sign == "-") ? -n : n;
                    jmpNopCounter++;
                    break;
                case "acc":
                    acc += (sign == "-") ? -n : n;
                    break;
                case "nop":
                    jmpNopCounter++;
                default:
                    break;
            }
        }
        if(finished)
        {
            break;
        }
    }

    return acc;
}
