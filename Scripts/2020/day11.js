function getSeatStatePart1(x, y, lines) {
    let currentSeat = lines[x][y];
    if (currentSeat == ".") {
        return { seat: ".", changed: false };
    }

    let occupiedSeatCount = 0;
    for (let i = Math.max(x - 1, 0); i < Math.min(x + 2, lines.length); i++) {
        for (let j = Math.max(y - 1, 0); j < Math.min(y + 2, lines[0].length); j++) {
            if (i == x && j == y) {
                continue;
            }
            if (lines[i][j] == "#") {
                occupiedSeatCount++;
            }
            if (currentSeat == "L" && occupiedSeatCount > 0) {
                return { seat: "L", changed: false };
            }
            if (currentSeat == "#" && occupiedSeatCount >= 4) {
                return { seat: "L", changed: true };
            }
        }
    }
    if (currentSeat == "L") {
        return { seat: "#", changed: true };
    }
    if (currentSeat == "#") {
        return { seat: "#", changed: false };
    }

    console.error("Something went wrong.");
    return { seat: ".", changed: false };
}

function runRoundPart1(lines) {
    let newLines = [];
    let stateChanged = false;
    for (let i = 0; i < lines.length; i++) {
        let line = lines[i];
        newLines.push("");
        for (let j = 0; j < line.length; j++) {
            let seatData = getSeatStatePart1(i, j, lines);
            if (seatData.changed) {
                stateChanged = true;
            }
            newLines[i] += seatData.seat;
        }
    }
    return { layout: newLines, changed: stateChanged };
}

function f2020day11part1(input) {
    let lines = input.split("\n");

    while (true) {
        let roundData = runRoundPart1(lines);
        lines = roundData.layout;
        if (!roundData.changed) {
            break;
        }
    }

    let seatCount = 0;
    for (let line of lines) {
        for (let c of line) {
            if (c == "#") {
                seatCount++;
            }
        }
    }

    return seatCount;
}

function getSeatStatePart2(x, y, lines) {
    let currentSeat = lines[x][y];
    if (currentSeat == ".") {
        return { seat: ".", changed: false };
    }

    let occupiedSeatCount = 0;
    for (let i = -1; i < 2; i++) {
        for (let j = -1; j < 2; j++) {
            if (i == 0 && j == 0) {
                continue;
            }
            let newX = x + i;
            let newY = y + j;
            if (newX < 0 || newX >= lines.length || newY < 0 || newY >= lines[0].length) {
                continue;
            }
            while (newX + i >= 0 && newX + i < lines.length && newY + j >= 0 && newY + j < lines[0].length && lines[newX][newY] == ".") {
                newX += i;
                newY += j;
                if(lines[newX][newY] != ".")
                {
                    break;
                }
            }
            if (lines[newX][newY] == "#") {
                occupiedSeatCount++;
            }
            if (currentSeat == "L" && occupiedSeatCount > 0) {
                return { seat: "L", changed: false };
            }
            if (currentSeat == "#" && occupiedSeatCount >= 5) {
                return { seat: "L", changed: true };
            }
        }
    }
    if (currentSeat == "L") {
        return { seat: "#", changed: true };
    }
    if (currentSeat == "#") {
        return { seat: "#", changed: false };
    }

    console.error("Something went wrong.");
    return { seat: ".", changed: false };
}

function runRoundPart2(lines) {
    let newLines = [];
    let stateChanged = false;
    for (let i = 0; i < lines.length; i++) {
        let line = lines[i];
        newLines.push("");
        for (let j = 0; j < line.length; j++) {
            let seatData = getSeatStatePart2(i, j, lines);
            if (seatData.changed) {
                stateChanged = true;
            }
            newLines[i] += seatData.seat;
        }
    }
    return { layout: newLines, changed: stateChanged };
}


function f2020day11part2(input) {
    let lines = input.split("\n");

    while (true) {
        let roundData = runRoundPart2(lines);
        lines = roundData.layout;
        if (!roundData.changed) {
            break;
        }
    }

    let seatCount = 0;
    for (let line of lines) {
        for (let c of line) {
            if (c == "#") {
                seatCount++;
            }
        }
    }

    return seatCount;
}