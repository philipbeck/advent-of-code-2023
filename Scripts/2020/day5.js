function f2020day5part1(input) {
    let lines = input.split("\n");

    let highestPass = 0;

    for (line of lines) {
        let row = line.substring(0, 7);
        row = row.replace(/F/g, "0");
        row = row.replace(/B/g, "1");
        row = "0b" + row;

        let column = line.substring(7, 10);
        column = column.replace(/L/g, "0");
        column = column.replace(/R/g, "1");
        column = "0b" + column;

        let passValue = Number(row) * 8 + Number(column);
        if(passValue > highestPass)
        {
            highestPass = passValue;
        }
    }
    return highestPass;
}

function f2020day5part2(input) {
    let lines = input.split("\n");

    let seats = [];
    for (line of lines) {
        let row = line.substring(0, 7);
        row = row.replace(/F/g, "0");
        row = row.replace(/B/g, "1");
        row = "0b" + row;
        
        let column = line.substring(7, 10);
        column = column.replace(/L/g, "0");
        column = column.replace(/R/g, "1");
        column = "0b" + column;

        let passValue = Number(row) * 8 + Number(column);
        seats.push(passValue);
    }
    seats.sort((a, b) => a - b);
    for(let i = 0; i < seats.length; i++)
    {
        if(seats[i+1] != seats[i] + 1)
        {
            return seats[i] + 1;
        }
    }

    return "Something went wrong";
}
