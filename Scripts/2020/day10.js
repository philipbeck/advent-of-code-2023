function f2020day10part1(input) {
    let lines = input.split("\n");
    lines.sort((a, b) => Number(a) - Number(b));
    console.log(lines);
    let oneDifferences = 1;
    let threeDifferences = 1;
    for (let i = 0; i < lines.length - 1; i++) {
        let diff = Number(lines[i + 1]) - Number(lines[i]);
        if (diff == 3) {
            threeDifferences++;
        }
        else if (diff == 1) {
            oneDifferences++;
        }
    }

    console.log("one " + oneDifferences + " three " + threeDifferences);
    return oneDifferences * threeDifferences;
}


// recursive function to calculate the permutations
function calculatePermutations(n, gap) {
    if (gap >= 4) {
        return 0;
    }
    if (n <= 0) {
        return 1;
    }
    let count = 0;
    count += calculatePermutations(n - 1, gap + 1);
    count += calculatePermutations(n - 1, 1);
    return count;
}

// uses permutation array so it only needs to call calculatePermutations once per n
var permutationArray = []
function getPermutations(n) {
    if (n == -1) {
        return 1;
    }
    if (n < permutationArray.length) {
        return permutationArray[n];
    }
    else {
        while (n >= permutationArray.length) {
            permutationArray.push(calculatePermutations(permutationArray.length, 1));
        }
    }

    return permutationArray[n];
}

function f2020day10part2(input) {
    let lines = input.split("\n");
    lines.push(0);
    lines.sort((a, b) => Number(a) - Number(b));
    lines.push(Number(lines[lines.length - 1]) + 3);

    console.log(lines);

    let singleGapCount = -1;
    let totalPermutations = 1;
    for (let i = 1; i < lines.length; i++) {
        let diff = Number(lines[i]) - Number(lines[i - 1]);
        if (diff == 1) {
            singleGapCount++;
        }
        if (diff == 3) {
            totalPermutations *= getPermutations(singleGapCount);
            singleGapCount = -1;
        }
    }

    return totalPermutations;
}