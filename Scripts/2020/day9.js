function f2020day9part1(input) {
    let lines = input.split("\n");
    for (let i = 25; i < lines.length; i++) {
        console.log(i);
        let iNum = Number(lines[i]);
        let sumExists = false;
        sumcheck:
        for (let j = i - 1; j > i - 26; j--) {
            let jNum = Number(lines[j]);
            for (let k = j - 1; k > i - 26; k--) {
                let kNum = Number(lines[k]);
                if(iNum == (jNum + kNum))
                {
                    sumExists = true;
                    break sumcheck;
                }
            }
        }
        if(!sumExists)
        {
            return iNum;
        }
    }

    return "Something went wrong";
}

function f2020day9part2(input) {
    let lines = input.split("\n");

    let target = f2020day9part1(input);

    for (let i = 0; i < lines.length; i++) {
        let total = 0;
        let min = Number.MAX_VALUE;
        let max = -1;
        for(let j = i; j < lines.length && total < target; j++)
        {
            let num = Number(lines[j]);
            total += num;
            if(num < min)
            {
                min = num;
            }
            if(num > max)
            {
                max = num;
            }

            if(total == target)
            {
                return min + max;
            }
        }
    }

    return "Not Implemented";
}
