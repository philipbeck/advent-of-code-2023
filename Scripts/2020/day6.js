function f2020day6part1(input) {
    let lines = input.split("\n");

    let total = 0;
    let map = new Map();
    for (line of lines) {
        if (line == "") {
            total += map.size;
            map = new Map();
        }

        for (let c of line) {
            map.set(c, true);
        }
    }
    total += map.size;
    return total;
}

function countCorrectAnswers(map, counter)
{
    let count = 0;
    for(let value of map)
    {
        if(value[1] == counter)
        {
            count++;
        }           
    }
    return count;
}

function f2020day6part2(input) {
    let lines = input.split("\n");

    let total = 0;
    let map = new Map();
    let counter = 0;
    for (line of lines) {
        if (line == "") {
            total += countCorrectAnswers(map, counter);
            map = new Map();
            counter = 0;
            continue;
        }

        for (let c of line) {
            let count = 1;
            if(map.has(c))
            {
                count = map.get(c) + 1;
            }
            map.set(c, count);
        }
        console.log(counter + " " + line);
        counter++;
    }
    total += countCorrectAnswers(map, counter);
    return total;
}