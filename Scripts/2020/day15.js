function runGame(input, count)
{
    let lines = input.split("\n");
    let numbers = lines[0].split(",");
    let numberToLastTurn = new Map();
    let lastNumber = -1;
    for (let i = 0; i < count; i++) {
        let turn = i + 1;
        if (i < numbers.length) {
            numberToLastTurn.set(Number(lastNumber), turn - 1);
            lastNumber = Number(numbers[i]);
        }
        else if (!numberToLastTurn.has(lastNumber)) {
            numberToLastTurn.set(lastNumber, turn - 1);
            lastNumber = 0;
        }
        else {
            let lastSpoken = numberToLastTurn.get(lastNumber);
            numberToLastTurn.set(lastNumber, turn - 1);
            lastNumber = turn - 1 - lastSpoken;
        }
    }
    return lastNumber;
}

function f2020day15part1(input) {
    return runGame(input, 2020);
}

function f2020day15part2(input) {
    return runGame(input, 30000000);
}
