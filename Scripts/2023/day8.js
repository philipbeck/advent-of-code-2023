function f2023day8part1(input) {
    let lines = input.split("\n");
    let directions = lines[0];
    let nodes = new Map();
    for (let i = 2; i < lines.length; i++) {
        let values = lines[i].split(/\W+/g);
        nodes.set(values[0], { L: values[1], R: values[2] });
    }

    let currentNode = "AAA";
    let count = 0;
    while (true) {
        let direction = directions[count % directions.length];
        count++;

        currentNode = nodes.get(currentNode)[direction];
        if (currentNode == "ZZZ") {
            break;
        }
    }

    return count;
}

function f2023day8part2(input) {
    let lines = input.split("\n");
    let directions = lines[0];
    let nodes = new Map();
    for (let i = 2; i < lines.length; i++) {
        let values = lines[i].split(/\W+/g);
        nodes.set(values[0], { L: values[1], R: values[2] });
    }

    let currentNodes = [];
    nodes.forEach((value, key) => {
        if (key[2] == "A") {
            currentNodes.push(key);
        }
    });

    let total = 1;
    for (let i = 0; i < currentNodes.length; i++) {
        let count = 0;

        while (true) {
            let directionPos = count % directions.length;
            let direction = directions[directionPos];

            currentNodes[i] = nodes.get(currentNodes[i])[direction];

            if (currentNodes[i][2] == "Z") {
                // the value is always a prime number of loops different to all the other
                // so we don't need to factorise or anything
                total *= ((count + 1)/directions.length);
                break;
            }
            count++;
        }
    }

    return total * directions.length;
}