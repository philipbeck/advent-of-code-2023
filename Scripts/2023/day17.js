var pathValues;

class MapNode {
    // age is how long it has been travelling in a particular directions
    constructor(x, y, direction, age, value) {
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.age = age;
        this.value = value;
    }

    getAgeDirectionString() {
        return "" + this.direction[0] + "" + this.direction[1] + "" + this.age;
    }
}

function getNodeInDirection(node, direction, lines, newAge) {
    let x = node.x + direction[0];
    let y = node.y + direction[1];
    if (x >= 0 && x < lines.length && y >= 0 && y < lines[0].length) {
        let newValue = Number(lines[x][y]) + node.value;
        let key = "" + direction[0] + "" + direction[1] + "" + newAge;
        if (!pathValues[x][y].has(key) || newValue < pathValues[x][y].get(key)) {
            pathValues[x][y].set(key, newValue);
            return new MapNode(x, y, direction, newAge, newValue);
        }
    }
    return undefined;
}

function getNext(node, lines, minAge, maxAge) {
    if (pathValues[node.x][node.y].has(node.getAgeDirectionString())
        && node.value != pathValues[node.x][node.y].get((node.getAgeDirectionString()))) {
        return [];
    }

    let returnNodes = [];
    if (node.age <= maxAge) {
        let newNode = getNodeInDirection(node, node.direction, lines, node.age + 1);
        if (newNode) {
            returnNodes.push(newNode);
        }
    }

    //do new directions
    if (node.age >= minAge) {
        let newDirections = [];
        if (node.direction[0] == 0) {
            newDirections = [[1, 0], [-1, 0]];
        }
        else {
            newDirections = [[0, 1], [0, -1]];
        }

        for (let direction of newDirections) {
            let newNode = getNodeInDirection(node, direction, lines, 1);
            if (newNode) {
                returnNodes.push(newNode);
            }
        }
    }

    return returnNodes;
}

// Doesn't quite work
function f2023day17part1(input) {
    let lines = input.split("\n");

    pathValues = [];
    for (let i = 0; i < lines.length; i++) {
        pathValues.push([]);
        for (let j = 0; j < lines[0].length; j++) {
            pathValues[i].push(new Map());
        }
    }
    pathValues[0][0].set("000", 0);

    let nodes = [new MapNode(0, 0, [0, 1], 0, 0)];

    while (nodes.length > 0) {
        let node = nodes.splice(0, 1)[0];
        let newNodes = getNext(node, lines, 0, 2);
        for (let newNode of newNodes) {
            if (newNode.x == pathValues.length - 1 && newNode.y == pathValues[0].length - 1) {
                return newNode.value;
            }
            nodes.push(newNode);
        }
        nodes.sort((a, b) => a.value - b.value);
    }
    return "Something went wrong";
}

function f2023day17part2(input) {
    let lines = input.split("\n");

    pathValues = [];
    for (let i = 0; i < lines.length; i++) {
        pathValues.push([]);
        for (let j = 0; j < lines[0].length; j++) {
            pathValues[i].push(new Map());
        }
    }
    pathValues[0][0].set("001", 0);

    let nodes = [new MapNode(0, 0, [0, 1], 0, 0)];

    while (nodes.length > 0) {
        let node = nodes.splice(0, 1)[0];
        let newNodes = getNext(node, lines, 4, 9);
        for (let newNode of newNodes) {
            if (newNode.x == pathValues.length - 1 && newNode.y == pathValues[0].length - 1) {// && newNode.age >= 4) {
                console.log(newNode);
            }
            nodes.push(newNode);
        }
        nodes.sort((a, b) => a.value - b.value);
    }
    return "Something went wrong";
}