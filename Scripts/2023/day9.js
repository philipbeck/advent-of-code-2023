function getNext(numbers) {
    let allZero = true;
    for (let n of numbers) {
        if (n != 0) {
            allZero = false;
            break;
        }
    }

    if (allZero) {
        return 0;
    }

    let newNumbers = [];
    for (let i = 0; i < numbers.length - 1; i++) {
        newNumbers.push(numbers[i+1] - numbers[i]);
    }
    return Number(numbers[numbers.length - 1]) + getNext(newNumbers);
}

function f2023day9part1(input) {
    let lines = input.split("\n");

    let total = 0;
    for (let line of lines) {
        let numbers = line.split(" ");
        total  += getNext(numbers); 
    }
    return total;
}

function getPrevious(numbers) {
    let allZero = true;
    for (let n of numbers) {
        if (n != 0) {
            allZero = false;
            break;
        }
    }

    if (allZero) {
        return 0;
    }

    let newNumbers = [];
    for (let i = 0; i < numbers.length - 1; i++) {
        newNumbers.push(numbers[i+1] - numbers[i]);
    }
    console.log(newNumbers);
    return Number(numbers[0]) - getPrevious(newNumbers);
}

function f2023day9part2(input) {
    let lines = input.split("\n");

    let total = 0;
    for (let line of lines) {
        let numbers = line.split(" ");
        total  += getPrevious(numbers); 
    }
    return total;
}