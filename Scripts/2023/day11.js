function replaceChar(str, c, i) {
    return str.substring(0, i) + c + str.substring(i + 1);
}
function addChar(str, c, i) {
    return str.substring(0, i) + c + str.substring(i);
}

function f2023day11part1(input) {
    let lines = input.split("\n");

    let emptyRow = "";
    for (let c of lines[0]) {
        // different character for identifying later
        emptyRow += "-";
    }
    // add rows
    for (let i = 0; i < lines.length; i++) {
        let line = lines[i];
        let empty = true;
        for (let c of line) {
            if (c == "#") {
                empty = false;
                break;
            }
        }
        if (empty) {
            lines.splice(i, 0, emptyRow);
            i++;
        }
    }

    // columns
    for (let i = 0; i < lines[0].length; i++) {
        let empty = true;
        for (line of lines) {
            if (line[i] == "#") {
                empty = false;
                break;
            }
        }
        if (empty) {
            for (let j = 0; j < lines.length; j++) {
                lines[j] = addChar(lines[j], "-", i);
            }
            i++;
        }
    }
    // keep positions of #s
    let galaxyArray = [];
    for (let i = 0; i < lines.length; i++) {
        for (let j = 0; j < lines[0].length; j++) {
            if (lines[i][j] == "#") {
                galaxyArray.push([i, j]);
            }
        }
    }

    // sum the difference between all the galaxies
    let total = 0;
    for (let i = 0; i < galaxyArray.length; i++) {
        for (let j = i + 1; j < galaxyArray.length; j++) {
            total += Math.abs(galaxyArray[i][0] - galaxyArray[j][0]) + Math.abs(galaxyArray[i][1] - galaxyArray[j][1]);
        }
    }

    return total;
}

function f2023day11part2(input) {
    let lines = input.split("\n");

    let emptyRow = "";
    for (let c of lines[0]) {
        // different character for identifying later
        emptyRow += "-";
    }
    // add rows
    for (let i = 0; i < lines.length; i++) {
        let line = lines[i];
        let empty = true;
        for (let c of line) {
            if (c == "#") {
                empty = false;
                break;
            }
        }
        if (empty) {
            lines.splice(i, 1, emptyRow);
        }
    }

    // columns
    for (let i = 0; i < lines[0].length; i++) {
        let empty = true;
        for (line of lines) {
            if (line[i] == "#") {
                empty = false;
                break;
            }
        }
        if (empty) {
            for (let j = 0; j < lines.length; j++) {
                lines[j] = replaceChar(lines[j], "-", i);
            }
        }
    }

    // keep positions of #s
    let galaxyArray = [];
    for (let i = 0; i < lines.length; i++) {
        for (let j = 0; j < lines[0].length; j++) {
            if (lines[i][j] == "#") {
                galaxyArray.push([i, j]);
            }
        }
    }

    let total = 0;

    for (let i = 0; i < galaxyArray.length; i++) {
        for (let j = i + 1; j < galaxyArray.length; j++) {
            let minX = Math.min(galaxyArray[i][0], galaxyArray[j][0]);
            let maxX = Math.max(galaxyArray[i][0], galaxyArray[j][0]);
            let minY = Math.min(galaxyArray[i][1], galaxyArray[j][1]);
            let maxY = Math.max(galaxyArray[i][1], galaxyArray[j][1]);
            for (let x = minX; x < maxX; x++) {
                if (lines[x][minY] == "-") {
                    total += 1000000;
                }
                else {
                    total++;
                }
            }
            for (let y = minY; y < maxY; y++) {
                if (lines[minX][y] == "-") {
                    total += 1000000;
                }
                else {
                    total++;
                }
            }
        }
    }

    return total;
}