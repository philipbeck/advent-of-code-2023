function f2023day6part1(input) {
  let lines = input.split("\n");
  times = lines[0].split(/\D+/);
  times.splice(0, 1);
  distances = lines[1].split(/\D+/);
  distances.splice(0, 1);

  let answer = 1;
  for (let i = 0; i < times.length; i++) {
    let time = times[i];
    let total = 0;
    for (let chargeTime = 0; chargeTime < time; chargeTime++) {
      if (chargeTime * (time - chargeTime) > distances[i]) {
        total++;
      }
    }
    answer *= total;
  }

  return answer;
}

// Not sure if this is 100% accurate but it works for all of the data I've had access to
function f2023day6part2(input) {
  let lines = input.split("\n");
  // turns the strings into single numbers
  let time = lines[0].replace(/\D+/g, "");
  let distance = lines[1].replace(/\D*/g, "");
  
  // Work out one of the points where the games start being run with recursion
  let chargeTime = 1;
  for (let i = 0; i < 30; i++) {
    let previousTime = chargeTime;
    chargeTime = time - distance / chargeTime;
    // if the numbers stop changing the answer can't be any more accurate
    if(previousTime == chargeTime)
    {
      break;
    }
  }
  if (chargeTime > time / 2) {
    chargeTime = time - chargeTime;
  }
  chargeTime = Math.ceil(chargeTime);

  let wins = time - chargeTime * 2;
  if (time % 2 == 0) {
    wins++;
  }

  return wins;
}