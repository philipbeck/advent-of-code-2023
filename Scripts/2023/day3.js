function checkAboveAndBelow(lines, lineNumber, pos) {
    if (lineNumber < lines.length - 1 && lines[lineNumber + 1][pos] != ".") {
        return true;
    }
    if (lineNumber > 0 && lines[lineNumber - 1][pos] != ".") {
        return true;
    }
    return false;
}

function getNumber(lines, lineNumber, pos) {
    let line = lines[lineNumber];
    let numberString = "";
    let isPart = false;
    if (pos > 0) {
        isPart = checkAboveAndBelow(lines, lineNumber, pos - 1);
        if (!isPart) {
            isPart = (line[pos - 1] != ".");
        }
    }
    while (pos < line.length) {
        if (!isPart) {
            isPart = checkAboveAndBelow(lines, lineNumber, pos);
        }
        if (line[pos].match(/\d/)) {
            numberString += line[pos];
            pos++;
        }
        else {
            break;
        }
    }
    if (!isPart && pos < line.length) {
        isPart = checkAboveAndBelow(lines, lineNumber, pos);
        if (!isPart) {
            isPart = (lines[lineNumber][pos] != ".");
        }
    }
    return { n: numberString, isPart: isPart };
}

function f2023day3part1(input) {
    let lines = input.split("\n");

    total = 0;
    for (let i = 0; i < lines.length; i++) {
        let line = lines[i];
        for (let j = 0; j < line.length; j++) {
            //check for number
            if (line[j].match(/\d/)) {
                let value = getNumber(lines, i, j);
                if (value.isPart) {
                    total += Number(value.n);
                }
                // minus 1 because the loop will immediately increment it
                j += value.n.length - 1;
            }
        }
    }

    return total;
}

function getNumber2(line, pos) {
    while (true) {
        if (!line[pos].match(/\d/)) {
            pos++;
            break;
        }
        if (pos <= 0) {
            break;
        }
        pos--;
    }
    let n = "";
    while (true) {
        if (pos >= line.length || !line[pos].match(/\d/)) {
            return n;
        }
        n += line[pos];
        pos++;
    }
}

function checkLineNextToGear(line, pos) {
    let numbers = [];
    if (pos == 0) { // one number max
        for (let i = 0; i < 2; i++) {
            if (line[i].match(/\d/)) {
                numbers.push(getNumber2(line, i));
                break;
            }
        }
    }
    else if (pos == line.length - 1) {
        for (let i = line.length - 2; i < line.length; i++) {
            if (line[i].match(/\d/)) {
                numbers.push(getNumber2(line, i));
                break;
            }
        }
    }
    else if (line[pos - 1].match(/\d/) && line[pos] == "." && line[pos + 1].match(/\d/)) {
        numbers.push(getNumber2(line, pos - 1));
        numbers.push(getNumber2(line, pos + 1));
    }
    else {
        for (let i = pos - 1; i < pos + 2; i++) {
            if (line[i].match(/\d/)) {
                numbers.push(getNumber2(line, i));
                break;
            }
        }
    }
    return numbers;
}

function getGear(lines, lineNumber, pos) {
    // Check top
    let gears = [];
    if (lineNumber > 0) {
        let values = checkLineNextToGear(lines[lineNumber - 1], pos);
        for (let value of values) {
            gears.push(value);
        }
    }
    if (lineNumber < lines.length - 1) {
        let values = checkLineNextToGear(lines[lineNumber + 1], pos);
        for (let value of values) {
            gears.push(value);
        }
    }
    let line = lines[lineNumber];
    if (pos > 0 && line[pos - 1].match(/\d/)) {
        gears.push(getNumber2(line, pos - 1));
    }
    if (pos < line.length - 1 && line[pos + 1].match(/\d/)) {
        gears.push(getNumber2(line, pos + 1));
    }
    if (gears.length == 2) {
        console.log(gears[0] + " " + gears[1]);
        return Number(gears[0]) * Number(gears[1]);
    }
    return 0;
}

function f2023day3part2(input) {
    let lines = input.split("\n");

    total = 0;
    for (let i = 0; i < lines.length; i++) {
        let line = lines[i];
        for (let j = 0; j < line.length; j++) {
            if (line[j] == "*") {
                let g = getGear(lines, i, j);
                total += g;
            }
        }
    }
    return total;
}