function f2023day1part1(input) {
    let lines = input.split("\n");

    let total = 0;
    for (let line of lines) {
        let first = -1;
        let last = 0;
        for (let c of line) {
            let n = Number(c);
            if (!isNaN(n)) {
                console.log(n)
                if (first == -1) {
                    first = n;
                }
                last = n;
            }
        }

        console.log("line: " + line + " number: " + first + "" + last);
        total += Number(first + "" + last);
    }
    return total;
}

const NUMBERS = [
    '1',
    'one',
    '2',
    'two',
    '3',
    'three',
    '4',
    'four',
    '5',
    'five',
    '6',
    'six',
    '7',
    'seven',
    '8',
    'eight',
    '9',
    'nine',
]

function checkNumber(pos, line)
{
    let subString = line.substring(pos);
    for (let i = 0; i < NUMBERS.length; i++) {
        let numberString = subString.substring(0, NUMBERS[i].length);
        if (numberString === NUMBERS[i]) {
            if (numberString.length > 1) {
                numberString = NUMBERS[i - 1];
            }
            return Number(numberString);
        }
    }
    return NaN;
}

function f2023day1part2(input) {
    let lines = input.split("\n");

    let total = 0;
    for (let line of lines) {
        let first = -1;
        let last = 0;
        for (let i = 0; i < line.length; i++) {
            let n = checkNumber(i, line);
            if (!isNaN(n)) {
                if (first == -1) {
                    first = n;
                }
                last = n;
            }
        }
        total += Number(first + "" + last)
    }
    return total;
}