const BAG = {
    red: 12,
    blue: 14,
    green: 13,
}

function f2023day2part1(input) {
    let lines = input.split("\n");

    let total = 0;

    for (let i = 0; i < lines.length; i++) {
        let possible = true;
        games = lines[i].split(":")[1].split(";");
        gameFor:
        for (game of games) {
            let rounds = game.split(",")
            for (round of rounds) {
                let splitString = round.split(" ");
                if (Number(splitString[1]) > BAG[splitString[2]]) {
                    possible = false;
                    break gameFor;
                }
            }
        }
        if (possible) {
            total += (i + 1);
        }
    }

    return total;
}

function f2023day2part2(input) {
    let lines = input.split("\n");

    let total = 0;

    for (let i = 0; i < lines.length; i++) {
        let mins = { red: 0, green: 0, blue: 0 }
        games = lines[i].split(":")[1].split(";");
        for (game of games) {
            let rounds = game.split(",")
            for (round of rounds) {
                let splitString = round.split(" ");
                if (mins[splitString[2]] < Number(splitString[1])) {
                    mins[splitString[2]] = Number(splitString[1]);
                }
            }
        }
        total += mins.red * mins.blue * mins.green;
    }

    return total;
}