// first number is up and down and second is left and right to match
// the lines array
const CONNECTIONS =
{
    "|": [[1, 0], [-1, 0]],
    "-": [[0, 1], [0, -1]],
    "L": [[0, 1], [-1, 0]],
    "J": [[0, -1], [-1, 0]],
    "7": [[0, -1], [1, 0]],
    "F": [[0, 1], [1, 0]],
}

function findStart(lines) {
    for (let i = 0; i < lines.length; i++) {
        for (let j = 0; j < lines[i].length; j++) {
            if (lines[i][j] == "S") {
                return [i, j];
            }
        }
    }
}

function findConnected(lines, startPos) {
    // check all the directions
    if (startPos[0] - 1 >= 0) {

        let character = lines[startPos[0] - 1][startPos[1]];

        if (character != ".") {
            for (let i = 0; i < 2; i++) {
                let coords = CONNECTIONS[character][i];
                if (coords[0] == 1) {
                    return CONNECTIONS[character][i == 0 ? 1 : 0];
                }
            }
        }
    }
    if (startPos[0] + 1 < lines.length) {
        let character = lines[startPos[0] + 1][startPos[1]];

        if (character != ".") {
            for (let i = 0; i < 2; i++) {
                let coords = CONNECTIONS[character][i];
                if (coords[0] == -1) {
                    return CONNECTIONS[character][i == 0 ? 1 : 0];
                }
            }
        }
    }
    if (startPos[1] - 1 >= 0) {
        let character = lines[startPos[0]][startPos[1] - 1];

        if (character != ".") {
            for (let i = 0; i < 2; i++) {
                let coords = CONNECTIONS[character][i];
                if (coords[1] == 1) {
                    return CONNECTIONS[character][i == 0 ? 1 : 0];
                }
            }
        }
    }
    if (startPos[1] + 1 < lines.length) {
        let character = lines[startPos[0]][startPos[1] + 1];

        if (character != ".") {
            for (let i = 0; i < 2; i++) {
                let coords = CONNECTIONS[character][i];
                if (coords[1] == -1) {
                    return CONNECTIONS[character][i == 0 ? 1 : 0];
                }
            }
        }
    }
    return [-2, -2];
}

function compareArrays(a1, a2) {
    if (a1.length != a2.length) {
        return false;
    }
    for (let i = 0; i < a1.length; i++) {
        if (a1[i] != a2[i]) {
            return false;
        }
    }
    return true;
}

function f2023day10part1(input) {
    let lines = input.split("\n");

    let start = findStart(lines);

    let nextDirection = findConnected(lines, start);

    let count = 1;

    let pos = [start[0] + nextDirection[0], start[1] + nextDirection[1]];
    nextDirection = [-nextDirection[0], -nextDirection[1]];

    while (lines[pos[0]][pos[1]] != "S") {
        for (coords of CONNECTIONS[lines[pos[0]][pos[1]]]) {
            if (!compareArrays(coords, nextDirection)) {
                nextDirection = [-coords[0], -coords[1]];
                pos = [pos[0] + coords[0], pos[1] + coords[1]];
                break;
            }
        }
        count++;
    }

    return Math.ceil(count / 2);
}

function replaceChar(str, c, i) {
    return str.substring(0, i) + c + str.substring(i + 1);
}

// this only works for input that doesn't touch the edge
function f2023day10part2(input) {
    let lines = input.split("\n");

    let start = findStart(lines);

    let nextDirection = findConnected(lines, start);

    let largeLines = [];
    for (let i = 0; i < lines.length; i++) {
        let s = "";
        for (let j = 0; j < lines[0].length; j++) {
            s += "..";
        }
        // Add it to the array twice
        largeLines.push(s);
        largeLines.push(s);
    }

    largeLines[start[0] * 2] = replaceChar(largeLines[start[0] * 2], "S", start[1] * 2);

    let pos = [start[0] + nextDirection[0], start[1] + nextDirection[1]];

    nextDirection = [-nextDirection[0], -nextDirection[1]];

    for (let coords of CONNECTIONS[lines[pos[0]][pos[1]]]) {
        if (!compareArrays(coords, nextDirection)) {
            largeLines[pos[0] * 2 + nextDirection[0]] = replaceChar(largeLines[pos[0] * 2 + nextDirection[0]], "E", pos[1] * 2 + nextDirection[1]);
        }
    }

    while (lines[pos[0]][pos[1]] != "S") {
        for (let coords of CONNECTIONS[lines[pos[0]][pos[1]]]) {
            if (!compareArrays(coords, nextDirection)) {
                // large lines
                largeLines[pos[0] * 2] = replaceChar(largeLines[pos[0] * 2], "#", pos[1] * 2);
                largeLines[pos[0] * 2 + coords[0]] = replaceChar(largeLines[pos[0] * 2 + coords[0]], "#", pos[1] * 2 + coords[1]);

                nextDirection = [-coords[0], -coords[1]];
                pos = [pos[0] + coords[0], pos[1] + coords[1]];
                break;
            }
        }
    }

    let positionsToCheck = new Map();
    positionsToCheck.set("" + [0, 0], [0, 0]);

    while (positionsToCheck.size != 0) {
        let nextPositions = new Map();

        positionsToCheck.forEach((value) => {
            // O for outside
            largeLines[value[0]] = replaceChar(largeLines[value[0]], "O", value[1]);

            if (value[0] + 1 < largeLines.length && largeLines[value[0] + 1][value[1]] == ".") {
                nextPositions.set("" + [value[0] + 1, value[1]], [value[0] + 1, value[1]]);
            }
            if (value[0] - 1 >= 0 && largeLines[value[0] - 1][value[1]] == ".") {
                nextPositions.set("" + [value[0] - 1, value[1]], [value[0] - 1, value[1]]);
            }
            if (value[1] + 1 < largeLines[0].length && largeLines[value[0]][value[1] + 1] == ".") {
                nextPositions.set("" + [value[0], value[1] + 1], [value[0], value[1] + 1]);
            }
            if (value[1] - 1 >= 0 && largeLines[value[0]][value[1] - 1] == ".") {
                nextPositions.set("" + [value[0], value[1] - 1], [value[0], value[1] - 1]);
            }
        });

        positionsToCheck.clear();
        positionsToCheck = nextPositions;
    }

    let counter = 0;
    for (let i = 0; i < largeLines.length; i += 2) {
        for (let j = 0; j < largeLines[0].length; j += 2) {
            if (largeLines[i][j] == ".") {
                counter++;
            }
        }
    }

    return counter;
}