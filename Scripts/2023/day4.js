function f2023day4part1(input) {
    let lines = input.split("\n");

    let total = 0;
    for (let line of lines) {
        let numbers = line.split(":")[1].split("|");
        let winningNumbers = numbers[0].split(" ");
        let cardNumbers = numbers[1].split(" ");
        let map = new Map();
        // fill map
        for (numberString of winningNumbers) {
            let n = Number(numberString)
            if (!isNaN(n)) {
                map.set(n, true);
            }
        }
        let count = 0;
        for (numberString of cardNumbers) {
            let n = Number(numberString)
            if (numberString != "" && !isNaN(n) && map.has(n)) {
                count++;
            }
        }
        total += (count == 0 ? 0 : Math.pow(2, count - 1));
    }

    return total;
}

function f2023day4part2(input) {
    let lines = input.split("\n");

    let total = 0;
    let copies = [0];
    for (let line of lines) {
        let numbers = line.split(":")[1].split("|");
        let winningNumbers = numbers[0].split(" ");
        let cardNumbers = numbers[1].split(" ");
        let map = new Map();
        // fill map
        for (numberString of winningNumbers) {
            let n = Number(numberString)
            if (!isNaN(n)) {
                map.set(n, true);
            }
        }
        let cardsScratched = 1 + copies[0];
        total += cardsScratched;
        copies = copies.splice(1);
        let count = 0;
        for (numberString of cardNumbers) {
            let n = Number(numberString)
            if (numberString != "" && !isNaN(n) && map.has(n)) {
                count++;
            }
        }
        for (let i = 0; i < count; i++) {
            if (!copies[i]) {
                copies.push(cardsScratched);
            }
            else {

                copies[i] += cardsScratched;
            }
        }
        console.log(copies);
        if (copies.length == 0) {
            copies = [0];
        }
    }
    return total;
}