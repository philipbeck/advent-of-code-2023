function setSeed(s, valueArray, index) {
    s.push(s[index]);
    for (value of valueArray) {
      if (
        s[index] >= value[1] &&
        s[index] <= Number(value[1]) + Number(value[2])
      ) {
        s[index + 1] = s[index] - Number(value[1]) + Number(value[0]);
        break;
      }
    }
  }
  
  function addNextMap(startPos, lines, seeds, index) {
    let valueArray = [];
    let i = startPos;
    for (; i < lines.length; i++) {
      if (lines[i] == "") {
        break;
      }
      let values = lines[i].split(" ");
      valueArray.push(values);
    }
  
    for (let s of seeds) {
      setSeed(s, valueArray, index);
    }
  
    return i;
  }
  
  function f2023day5part1(input) {
    let lines = input.split("\n");
  
    // line
    let seedStrings = lines[0].split(" ");
    let seeds = [];
    for (let s of seedStrings) {
      let n = Number(s);
      if (!isNaN(n)) {
        seeds.push([n]);
      }
    }
  
    let index = 0;
    for (let i = 2; i < lines.length; i++) {
      let line = lines[i];
      if (line.includes("map")) {
        console.log(line);
        i = addNextMap(i + 1, lines, seeds, index);
        index++;
      }
    }
    let min = Number.MAX_VALUE;
    for (let s of seeds) {
      console.log(JSON.stringify(s));
      if (s[index] < min) {
        min = s[index];
      }
    }
  
    return min;
  }
  
  function getNewRanges(range, valueArray) {
    if (valueArray.length == 0) {
      return [range];
    }
  
    let newRanges = [];
    let value = valueArray[0];
    let n1;
    let n2;
    let min = value[1];
    let max = Number(value[1]) + Number(value[2]) - 1;
    // range entirely covers the current rule
    if (range[0] < min && range[1] > max) {
      n1 = min - Number(value[1]) + Number(value[0]);
      n2 = max - Number(value[1]) + Number(value[0]);
      newRanges = [[n1, n2]];
      if (range[0] < min - 1) {
        newRanges = newRanges.concat(
          getNewRanges([range[0], min - 1], valueArray.slice(1))
        );
      }
      if (range[1] > max + 1) {
        newRanges = newRanges.concat(
          getNewRanges([max + 1, range[1]], valueArray.slice(1))
        );
      }
    }
    // Range is entirely within the rule
    else if (
      range[0] >= min &&
      range[0] <= max &&
      range[1] >= min &&
      range[1] <= max
    ) {
      n1 = range[0] - Number(value[1]) + Number(value[0]);
      n2 = range[1] - Number(value[1]) + Number(value[0]);
      newRanges = [[n1, n2]];
    }
    // the left is within the rule and the right is outside
    else if (range[0] >= min && range[0] <= max && range[1] > max) {
      n1 = range[0] - Number(value[1]) + Number(value[0]);
      n2 = max - Number(value[1]) + Number(value[0]);
      newRanges = [[n1, n2]];
      if (range[1] > max + 1) {
        newRanges = newRanges.concat(
          getNewRanges([max + 1, range[1]], valueArray.slice(1))
        );
      }
    }
    // the right is within the rule and the left is outside
    else if (range[1] >= min && range[1] <= max && range[0] < min) {
      n1 = min - Number(value[1]) + Number(value[0]);
      n2 = range[1] - Number(value[1]) + Number(value[0]);
      newRanges = [[n1, n2]];
      if (range[0] < min - 1) {
        newRanges = newRanges.concat(
          getNewRanges([range[0], min - 1], valueArray.slice(1))
        );
      }
    } else if (
      (range[0] < min && range[1] < min) ||
      (range[0] > max && range[1] > max)
    ) {
      newRanges = getNewRanges(range, valueArray.slice(1));
    }
    
    return newRanges;
  }
  
  function checkRanges(startPos, lines, ranges) {
    let newRanges = [];
  
    let valueArray = [];
    let i = startPos;
    for (; i < lines.length; i++) {
      if (lines[i] == "") {
        break;
      }
      let values = lines[i].split(" ");
      valueArray.push(values);
    }
  
    for (let r of ranges) {
      newRanges = newRanges.concat(getNewRanges(r, valueArray));
    }
  
    return { newPos: i, r: newRanges };
  }
  
  function f2023day5part2(input) {
    let lines = input.split("\n");
    let ranges = [];
    let seedStrings = lines[0].split(" ");
    for (let i = 1; i <= seedStrings.length; i += 2) {
      let n1 = Number(seedStrings[i]);
      let n2 = Number(seedStrings[i + 1]);
      if (!isNaN(n1) && !isNaN(n2)) {
        ranges.push([n1, n1 + n2 - 1]);
      } else {
      }
    }
  
    for (let i = 2; i < lines.length; i++) {
      let line = lines[i];
      if (line.includes("map")) {
        value = checkRanges(i + 1, lines, ranges);
        ranges = value.r;
        i = value.newPos;
      }
    }
    let min = Number.MAX_VALUE;
    for (range of ranges) {
      if (min > range[0]) {
        min = range[0];
      }
    }
  
    return min;
  }
  