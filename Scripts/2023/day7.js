function getCardValuePart1(card) {
    let cardNumber = Number(card);
    if (!isNaN(cardNumber)) {
        return cardNumber;
    }
    switch (card) {
        case "T":
            return 10;
        case "J":
            return 11;
        case "Q":
            return 12;
        case "K":
            return 13;
        case "A":
            return 14;
        default:
            console.error("unknown card " + card);
    }
}

class handPart1 {
    constructor(cards, bid) {
        this.bid = bid;

        let cardsTested = new Map();
        for (let i = 0; i < cards.length; i++) {
            let card = cards[i];
            if (cardsTested.has(card)) {
                let count = cardsTested.get(card);
                cardsTested.set(card, count + 1);
            }
            else {
                cardsTested.set(card, 1);
            }

        }
        let cardArray = Array.from(cardsTested, ([card, count]) => ({ card, count }));
        cardArray.sort((a, b) => ((b.count * 16) + getCardValuePart1(b.card)) - ((a.count * 16) + getCardValuePart1(a.card)));


        this.cards = [0, 0, 0, 0, 0];
        if (cardArray[0].count == 5) {
            this.order = 6;
        }
        else if (cardArray[0].count == 4) {
            this.order = 5;
        }
        else if (cardArray[0].count == 3 && cardArray[1].count == 2) {
            this.order = 4;
        }
        else if (cardArray[0].count == 3) {
            this.order = 3;
        }
        else if (cardArray[0].count == 2 && cardArray[1].count == 2) {
            this.order = 2;
        }
        else if (cardArray[0].count == 2) {
            this.order = 1;
        }
        else {
            this.order = 0;
        }

        for (let i = 0; i < cards.length; i++) {
            this.cards[i] = getCardValuePart1(cards[i]);
        }

        this.sortingValue = this.getHandSortingValue();
        if (this.order == 0) {
        }
    }

    getHandSortingValue() {
        let v = (this.order * 16);
        for (let card of this.cards) {
            v += getCardValuePart1(card);
            v *= 16;
        }
        return v;
    }
}

function f2023day7part1(input) {
    let lines = input.split("\n");

    let handList = [];
    for (line of lines) {
        let data = line.split(" ");
        handList.push(new handPart1(data[0], Number(data[1])));
    }
    handList.sort((a, b) => a.sortingValue - b.sortingValue);

    let total = 0;
    for (let i = 0; i < handList.length; i++) {
        let hand = handList[i];
        total += (i + 1) * hand.bid;
    }

    return total;
}

function getCardValuePart2(card) {
    let cardNumber = Number(card);
    if (!isNaN(cardNumber)) {
        return cardNumber;
    }
    switch (card) {
        case "T":
            return 10;
        case "J":
            return 0;
        case "Q":
            return 12;
        case "K":
            return 13;
        case "A":
            return 14;
        default:
            console.error("unknown card " + card);
    }
}

class handPart2 {
    constructor(cards, bid) {
        this.bid = bid;

        let cardsTested = new Map();
        for (let i = 0; i < cards.length; i++) {
            let card = cards[i];
            if (cardsTested.has(card)) {
                let count = cardsTested.get(card);
                cardsTested.set(card, count + 1);
            }
            else {
                cardsTested.set(card, 1);
            }

        }
        let cardArray = Array.from(cardsTested, ([card, count]) => ({ card, count }));
        cardArray.sort((a, b) => ((b.count * 16) + getCardValuePart2(b.card)) - ((a.count * 16) + getCardValuePart2(a.card)));

        let jokerIndex = cardArray.findIndex((a) => a.card == "J");
        if (jokerIndex > -1 && cardArray[jokerIndex].count < 5) {
            if (jokerIndex == 0) {
                cardArray[1].count += cardArray[jokerIndex].count;
            }
            else {
                cardArray[0].count += cardArray[jokerIndex].count;
            }
            cardArray.splice(jokerIndex, 1);
        }

        this.cards = [0, 0, 0, 0, 0];
        if (cardArray[0].count == 5) {
            this.order = 6;
        }
        else if (cardArray[0].count == 4) {
            this.order = 5;
        }
        else if (cardArray[0].count == 3 && cardArray[1].count == 2) {
            this.order = 4;
        }
        else if (cardArray[0].count == 3) {
            this.order = 3;
        }
        else if (cardArray[0].count == 2 && cardArray[1].count == 2) {
            this.order = 2;
        }
        else if (cardArray[0].count == 2) {
            this.order = 1;
        }
        else {
            this.order = 0;
        }

        for (let i = 0; i < cards.length; i++) {
            this.cards[i] = getCardValuePart2(cards[i]);
        }

        this.sortingValue = this.getHandSortingValue();
        if (this.order == 0) {
        }
    }

    getHandSortingValue() {
        let v = (this.order * 16);
        for (let card of this.cards) {
            v += getCardValuePart2(card);
            v *= 16;
        }
        return v;
    }
}

function f2023day7part2(input) {
    let lines = input.split("\n");

    let handList = [];
    for (line of lines) {
        let data = line.split(" ");
        handList.push(new handPart2(data[0], Number(data[1])));
    }
    handList.sort((a, b) => a.sortingValue - b.sortingValue);

    let total = 0;
    for (let i = 0; i < handList.length; i++) {
        let hand = handList[i];
        total += (i + 1) * hand.bid;
    }

    return total;
}